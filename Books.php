<?
require_once 'db.php';
class Books{
    private $db = $db;
    public function create($idAuthors, $name){
        $query = "INSERT INTO books (`name`) VALUES (?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($name);
        $stmt->execute();
        $idBook = $this->db->insert_id;
        foreach($idAuthors as $idAuthor){
            $query = "INSERT INTO authorsbooks (idBook, idAuthor) VALUES (?, ?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param($idBook, $idAuthor);
            $stmt->execute();
        }
    }
    public function read($idBook){
        $query = "SELECT b.name, a.name FROM books b JOIN authorsbooks ab ON b.ID = ab.idBook JOIN authors a ON a.ID = ab.idAuthor WHERE b.ID = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($idBook);
        $stmt->execute();
        $stmt->bind_result($bookName, $authorNames);
        return ['name' => $bookName[0], 'authors' => $authorNames];
    }
    public function update($idBook, $name){
        $query = "UPDATE `books` SET `name` = ? WHERE `ID` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($name, $idBook);
        $stmt->execute();
    }
    public function delete($idBook){
        $query = "DELETE FROM `books` WHERE `ID` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($idBook);
        $stmt->execute();
    }
    public function getAll(){
        $query = "SELECT `ID`, `name` FROM books";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $stmt->bind_result($idBooks, $names);
        $books = array_combine($idBooks, $names);
        $result = [];
        foreach($books as $idBook=>$name){
            $result[$idBook] = ['name'=>$name, 'authors'=>$this->getAuthors($idBook)];
        }
        return $result;
    }
    public function getAuthors($idBook){
        $query = "SELECT a.name FROM books b JOIN authorsbooks ab ON b.ID = ab.idBook JOIN authors a ON a.ID = ab.idAuthor WHERE b.ID = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($idBook);
        $stmt->execute();
        $stmt->bind_result($authorNames);
        return $authorNames;
    }
}
