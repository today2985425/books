<?
require_once 'db.php';
class Authors{
    private $db = $db;
    public function create($idBooks, $name){
        $query = "INSERT INTO authors (`name`) VALUES (?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($name);
        $stmt->execute();
        $idAuthor = $this->db->insert_id;
        foreach($idBooks as $idBook){
            $query = "INSERT INTO authorsbooks (idBook, idAuthor) VALUES (?, ?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param($idBook, $idAuthor);
            $stmt->execute();
        }
    }
    public function read($idAuthor){
        $query = "SELECT b.name, a.name FROM books b JOIN authorsbooks ab ON b.ID = ab.idBook JOIN authors a ON a.ID = ab.idAuthor WHERE a.ID = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($idAuthor);
        $stmt->execute();
        $stmt->bind_result($bookNames, $authorName);
        return ['name' => $authorName[0], 'books' => $bookNames];
    }
    public function update($idBook, $name){
        $query = "UPDATE `authors` SET `name` = ? WHERE `ID` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($name, $idBook);
        $stmt->execute();
    }
    public function delete($idBook){
        $query = "DELETE FROM `authors` WHERE `ID` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param($idBook);
        $stmt->execute();
    }
    public function getBooksCount(){
        $query = "SELECT a.name, COUNT(idBook) FROM authors a JOIN authorsbooks ab ON b.ID = ab.idBook GROUP BY ab.idAuthor";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $stmt->bind_result($names, $counts);
        return array_combine($names, $counts);
    }
}
