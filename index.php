<?php
require_once 'Books.php';
require_once 'Authors.php';
$uri = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];
switch($method){
    case 'GET':
        switch($uri){
            case '/api/v1/books/list':
                $books = new Books();
                echo json_encode(['success'=>true, 'result' => $books->getAll()]);
                exit();
                break;

            default:
                //например '/api/v1/books/by-id/17'
                $a = explode ('/api/v1/books/by-id/', $uri);
                if((count($a) == 2)&&($a[0] == '')&&$a[1] > 0){
                    $books = new Books();
                    echo json_encode(['success'=>true, 'result' => $books->read($a[1])]);
                    exit();
                }
                    else {
                        echo json_encode(['success'=>false, 'info' => 'Address Error']);
                        exit();
                    }
            break;
        }
        break;
    case 'POST':
        switch($uri){
            case '/api/v1/books/update':
                if(!isset($_POST['id'])){
                    echo json_encode(['success'=>false, 'info' => 'Please provide id']);
                    exit();
                }
                if(!isset($_POST['name'])){
                    echo json_encode(['success'=>false, 'info' => 'Please provide name']);
                    exit();
                }
                $books = new Books();
                $books->update($_POST['id'], $_POST['name']);
                echo json_encode(['success'=>true]);
                exit();
                break;
            default:
                echo json_encode(['success'=>false, 'info' => 'Address Error']);
                exit();
                break;
        }
        break;
    case 'DELETE':
        switch($uri){
            case '/api/v1/books/id':
                if(!isset($_POST['id'])){
                    echo json_encode(['success'=>false, 'info' => 'Please provide id']);
                    exit();
                }
                $books = new Books();
                $books->delete($_POST['id']);
                echo json_encode(['success'=>true]);
                exit();
                break;
            default:
                echo json_encode(['success'=>false, 'info' => 'Address Error']);
                break;
        }
        break;
    default:
        echo json_encode(['success'=>false, 'info' => 'Address Error']);
        exit();
        break;
}